package pruebaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {

	public static void main(String[] args) {
		
		//cargar el xml de config
		ClassPathXmlApplicationContext contexto=new ClassPathXmlApplicationContext("applicationContext.xml");

		//pedir un bean al contendor
		Empleados Antonio=contexto.getBean("comercianteExperimentado", Empleados.class);
		
		//usar el bean
		System.out.println(Antonio.getInforme());
		System.out.println(Antonio.getTareas());
		
		
		//cerrar el contexto
		contexto.close();

		
	}

}
