package pruebaAnnotations;

import org.springframework.beans.factory.annotation.Value;

public class DirectorFinanciero implements Empleados {
	
	public DirectorFinanciero(CreacionInformeFinanciero informeFinanciero) {
		this.informeFinanciero = informeFinanciero;
	}
		
	
	
	@Override
	public String getTareas() {
		return "Gestion y Direccion de las Operaciones Financieras de la Empresa";
	}

	@Override
	public String getInforme() {
		return informeFinanciero.getInformeFinanciero();
	}
	
	
	public String getEmail() {
		return email;
	}


	public String getNombreEmpresa() {
		return nombreEmpresa;
	}



	@Value("${email}")		//vid25
	private String email;
	@Value("${nombreEmpresa}")	//vid25
	private String nombreEmpresa;
	
	private CreacionInformeFinanciero informeFinanciero;

	

}
