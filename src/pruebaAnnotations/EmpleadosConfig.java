package pruebaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("pruebaAnnotations")
@PropertySource("classpath:datosEmpresa.propiedades")		//vid25
public class EmpleadosConfig {
	
	//definir el bean para InformeFinancieroDptoCompras
	
	@Bean
	public CreacionInformeFinanciero informeFinancieroDptoCompras() { //esto sera el id del bean inyectado
		return new InformeFinancieroDptoCompras();
	}
	
	//definir el  bean apra DirectorFinanciero e inyectar dependencias
	
	@Bean
	public Empleados directorFinanciero() {
		return new DirectorFinanciero(informeFinancieroDptoCompras());
	}

}
