package pruebaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations2 {

	public static void main(String[] args) {

		//cargar el xml de config
		//ClassPathXmlApplicationContext contexto=new ClassPathXmlApplicationContext("applicationContext.xml");
		//leer el class de configuracion
		AnnotationConfigApplicationContext contexto=new AnnotationConfigApplicationContext(EmpleadosConfig.class);
		
		//pedir un bean al contendor
		
		
		DirectorFinanciero empleado=contexto.getBean("directorFinanciero",DirectorFinanciero.class);
		System.out.println("Email del director: "+empleado.getEmail());
		System.out.println("Nombre de la Empresa: "+empleado.getNombreEmpresa());

		
		/*Empleados empleado = contexto.getBean("directorFinanciero",Empleados.class);
		System.out.println(empleado.getTareas());
		System.out.println(empleado.getInforme());
		*/
		
		/*Empleados Antonio=contexto.getBean("comercianteExperimentado", Empleados.class);
		Empleados Lucia=contexto.getBean("comercianteExperimentado", Empleados.class);

		
		if(Antonio==Lucia) {
			System.out.println("Apuntan a lo mismo");
			System.out.println(Antonio + "\n" + Lucia);

		}else{
			System.out.println("No Apuntan a lo mismo");
			System.out.println(Antonio + "\n" + Lucia);
		}*/
	
	
		
		//cerrar el contexto
		contexto.close();
		
	
	}

}
