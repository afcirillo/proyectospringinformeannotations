package pruebaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

@Component//Esto ahora es un bean //VID17 el bean automaticamente toma el nombre de la clase con minuscula
//@Scope("prototype") //vid21 Annotation de SCOPE para Prototype o Singleton
public class ComercianteExperimentado implements Empleados,InitializingBean, DisposableBean  { 

	
	//AUTOWIRED1 VERSION BASICA
	/*@Autowired//busca una clase q implemente CreacionInformeFinanciero 
				//y la utiliza para hacer la inyeccion
	public ComercianteExperimentado(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}*/

	
	
	@Override
	public String getTareas() {
		return "Vender, vender y vender mas ";
	}
	
	
	//AUTOWIRED2 VERSION SETTER
	/*@Autowired
	public void setNuevoInforme(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}*/

	@Override
	public String getInforme() {
		//return "Informe generado por el comercialr ";
		return nuevoInforme.getInformeFinanciero();
	}
	
	//AUTOWIRED3 VERSION "METODO CUALQUIERA O CAMPO DE CLASE O MAS SIMPLE"
	@Autowired
	@Qualifier("informeFinancieroTrim4") //vid20 se especifica el nombre de la clase 
	//con minuscula para que spring sepa cual de las 4 clases debe inyectar, ya que si no lo hago, da error.
	private CreacionInformeFinanciero nuevoInforme;
	
	
	
	//Ejecucion de Codigo despues de Creacion del Bean
	//@PostConstruct			//A PARTIR DE JAVA 9 EN ADELANTE ESTA ANNOTATION YA NO ANDA!!
	public void ejecutaDespuesCreacion() {
		System.out.println("Ejecutado tras creacion de bean");
	}
	
	
	//Ejecucion de Codigo despues de Apagado contenendor Spring
	//@PreDestroy			//A PARTIR DE JAVA 9 EN ADELANTE ESTA ANNOTATION YA NO ANDA!!
	public void ejecutaAntesDestruccion() {
		System.out.println("Ejecutado antes de la destruccion");
	}
	
	//vid22 SE REEMPLAZAN POR ESTO Y POR "implements InitializingBean, DisposableBean" en el nombre de la clase!!
	public void destroy() throws Exception {/*System.out.println("Ejecutado antes de la destruccion")*/;} 
	public void afterPropertiesSet() throws Exception {/*System.out.println("Ejecutado tras creacion de bean")*/;}


	
}
